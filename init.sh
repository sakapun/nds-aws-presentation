#!/usr/bin/env bash

docker build . -t pin-image-generator

docker run --rm -v $(pwd):/task --workdir=/task pin-image-generator yarn install
