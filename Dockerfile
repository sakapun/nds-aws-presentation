FROM node:8

# Installing graphicsmagick
RUN apt-get update && apt-get install -y --no-install-recommends \
			graphicsmagick \
		&& rm -rf /var/lib/apt/lists/*

RUN npm i -g typescript ts-node yarn

CMD ["ts-node", "index.ts"]
