import {AssetType} from './asset';
import * as fs from 'fs';
import axios from 'axios';
import {orderBy} from 'lodash';

// console.log(asset);



const buildPage = (row: AssetType) => {
  return `
# ${row.製品名.replace(/-/g, " ")}

![${row.製品名}](${row.リアル画像パス})

${row.説明}
`;
};

const sorterCategoryNone = (row: AssetType) => {
  return row.ソート == ""
};

const today = new Date();
const writeDay = process.env.WRITE_DAY || `${today.getFullYear()}/${today.getMonth() + 1}/${today.getDate()}`;
const mdPreContent = `
# AWSのサービスをできる限り説明する
@sakapun
${writeDay}

------------------

#自己紹介
名前：坂本将之、HNは@sakapun
所属：新潟の農業IT、ウォーターセル株式会社
好きなAWSサービス：Lambda, Amplify

------------------

AWS新しいアイコン集、全部抽出したら200ページ近くになりました！
グルーピングのアイコンもありますが、数はそんなもんです。
今日の資料はカテゴリのabc順で、説明しやすいように並べてきました。

------------------

# 説明してくぞ！

------------------`;

const mdPostContent = `
------------------

# まとめ

------------------

機械学習、IoT、サーバレス、AWS管理についてのラインナップが拡充しているなという感想。
それらは度々ピックアップされてるので、モテAWSエンジニアになるには抑えておいたほうがいい分野。

------------------

# 終わり
`;

(async () => {
  const response = await axios.get("https://script.google.com/macros/s/AKfycbxaY_9OPMtO6U4olG1lkL-k255GOoFI_RONhfgdw1zlMU_qHEYr/exec");
  const rows: AssetType[] = response.data as AssetType[];
  const orderedRows = orderBy(rows, ["分野", sorterCategoryNone, "ソート", "製品名"], ["asc", "desc" , "asc" ,"asc"]);
  const mdData = orderedRows
    .filter(r => r.ソート !== -1)
    .map(row => buildPage(row)).join("\n---------------\n");
  // console.log(mdData)

  fs.writeFileSync('./dist/build.md', mdPreContent + mdData + mdPostContent);

})();

