export type AssetType = {
  製品名: string,
  分野: string,
  画像パス: string,
  ダミー: string,
  リアル画像パス: string,
  説明: string,
  ソート: number | ""
}

export const asset: AssetType[]= [
  {
    "製品名": "Amazon-Athena",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Athena_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Athena_dark-bg@4x.png",
    "説明": "S3のデータに対してクエリで問い合わせできるやつ",
    "ソート": ""
  },
  {
    "製品名": "Amazon-CloudSearch",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-CloudSearch_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-CloudSearch_dark-bg@4x.png",
    "説明": "投入済みのデータに対しての検索。\n最近話を聞かない。\nelastisearchと似ている",
    "ソート": ""
  },
  {
    "製品名": "Amazon-CloudSearch_Search-documents",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-CloudSearch_Search-documents_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-CloudSearch_Search-documents_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Elasticsearch-Service",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Elasticsearch-Service_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Elasticsearch-Service_dark-bg@4x.png",
    "説明": "データに対して検索する。\n勝手に色々できる。インスタンスが必要。",
    "ソート": ""
  },
  {
    "製品名": "Amazon-EMR_Cluster",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-EMR_Cluster_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-EMR_Cluster_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-EMR",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-EMR_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-EMR_dark-bg@4x.png",
    "説明": "分散処理環境の",
    "ソート": ""
  },
  {
    "製品名": "Amazon-EMR_EMR-engine-MapR-M3",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-EMR_EMR-engine-MapR-M3_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-EMR_EMR-engine-MapR-M3_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-EMR_EMR-engine-MapR-M5",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-EMR_EMR-engine-MapR-M5_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-EMR_EMR-engine-MapR-M5_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-EMR_EMR-engine-MapR-M7",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-EMR_EMR-engine-MapR-M7_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-EMR_EMR-engine-MapR-M7_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-EMR_EMR-engine",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-EMR_EMR-engine_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-EMR_EMR-engine_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-EMR_HDFS-cluster",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-EMR_HDFS-cluster_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-EMR_HDFS-cluster_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Kinesis-Data-Analytics",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Kinesis-Data-Analytics_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Kinesis-Data-Analytics_dark-bg@4x.png",
    "説明": "ストリームに対してクエリを投げられる",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Kinesis-Data-Firehose",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Kinesis-Data-Firehose_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Kinesis-Data-Firehose_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Kinesis-Data-Streams",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Kinesis-Data-Streams_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Kinesis-Data-Streams_dark-bg@4x.png",
    "説明": "ストリームデータを保存する。Firehoseより早いが設定が難しい",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Kinesis-Video-Streams",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Kinesis-Video-Streams_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Kinesis-Video-Streams_dark-bg@4x.png",
    "説明": "撮ったデータをすぐに分析かけれる。例えば動画撮って顔の検出とか",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Kinesis",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Kinesis_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Kinesis_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Managed-Streaming-for-Kafka",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Managed-Streaming-for-Kafka_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Managed-Streaming-for-Kafka_dark-bg@4x.png",
    "説明": "kinesis streamの部分の低レイテンシでいっぱい捌ける。オープンソースのapatch kafka",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Quicksight",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Quicksight_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Quicksight_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Redshift",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Redshift_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Redshift_dark-bg@4x.png",
    "説明": "並列分散処理をする列志向データベース。\nデータウェアハウス。なのでデータの追加には向かず、バルク読込して解析に使う\n16PBまでの容量もてる。\nクラスタを常時上げておく必要がある",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Redshift_Dense-compute-node",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Redshift_Dense-compute-node_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Redshift_Dense-compute-node_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Redshift_Dense-storage-node",
    "分野": "Analytics",
    "画像パス": "Analytics/Amazon-Redshift_Dense-storage-node_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Redshift_Dense-storage-node_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Analytics",
    "分野": "Analytics",
    "画像パス": "Analytics/Analytics_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Analytics_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Data-Pipeline",
    "分野": "Analytics",
    "画像パス": "Analytics/AWS-Data-Pipeline_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/AWS-Data-Pipeline_dark-bg@4x.png",
    "説明": "データの移行サービス。具体的な事例として挙げられるのが定期的にRDSからRedshiftへのデータの注入が必要なとき。2015年",
    "ソート": ""
  },
  {
    "製品名": "AWS-Glue_Crawlers",
    "分野": "Analytics",
    "画像パス": "Analytics/AWS-Glue_Crawlers_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/AWS-Glue_Crawlers_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Glue",
    "分野": "Analytics",
    "画像パス": "Analytics/AWS-Glue_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/AWS-Glue_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Glue_Data-catalog",
    "分野": "Analytics",
    "画像パス": "Analytics/AWS-Glue_Data-catalog_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/AWS-Glue_Data-catalog_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Lake-Formation",
    "分野": "Analytics",
    "画像パス": "Analytics/AWS-Lake-Formation_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/AWS-Lake-Formation_dark-bg@4x.png",
    "説明": "glueを中心とした、分析環境を構築できる。glueのラッパー\n機能を使える人を制限する権限管理的な意味も含む\nカタログ化とラベル付け、データの変換",
    "ソート": ""
  },
  {
    "製品名": "Amazon-AppSync",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/Amazon-AppSync_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-AppSync_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-MQ",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/Amazon-MQ_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-MQ_dark-bg@4x.png",
    "説明": "Apatch activeMQの フルマネージドサービス MQ はメッセージキューイングサービスの略",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Simple-Notification-Service-SNS",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/Amazon-Simple-Notification-Service-SNS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-Simple-Notification-Service-SNS_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Simple-Notification-Service-SNS_Email-Notification",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/Amazon-Simple-Notification-Service-SNS_Email-Notification_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-Simple-Notification-Service-SNS_Email-Notification_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Simple-Notification-Service-SNS_HTTP-Notification",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/Amazon-Simple-Notification-Service-SNS_HTTP-Notification_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-Simple-Notification-Service-SNS_HTTP-Notification_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Simple-Notification-Service-SNS_Topic",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/Amazon-Simple-Notification-Service-SNS_Topic_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-Simple-Notification-Service-SNS_Topic_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Simple-Queue-Service-SQS",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/Amazon-Simple-Queue-Service-SQS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-Simple-Queue-Service-SQS_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Simple-Queue-Service-SQS_Message",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/Amazon-Simple-Queue-Service-SQS_Message_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-Simple-Queue-Service-SQS_Message_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Simple-Queue-Service-SQS_Queue",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/Amazon-Simple-Queue-Service-SQS_Queue_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-Simple-Queue-Service-SQS_Queue_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Application-Integration",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/Application-Integration_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Application-Integration_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Step-Functions",
    "分野": "Application Integration",
    "画像パス": "Application+Integration/AWS-Step-Functions_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/AWS-Step-Functions_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Sumerian",
    "分野": "AR & VR",
    "画像パス": "AR+&+VR/Amazon-Sumerian_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AR+&+VR/Amazon-Sumerian_dark-bg@4x.png",
    "説明": "AR 空間で3 D モデルをお絵かきしたりできるツール\nUnity をブラウザで使えるようなそんな感覚と言っていいんだろうか",
    "ソート": ""
  },
  {
    "製品名": "AR-VR",
    "分野": "AR & VR",
    "画像パス": "AR+&+VR/AR-VR_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AR+&+VR/AR-VR_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Budgets",
    "分野": "AWS Cost Management",
    "画像パス": "AWS+Cost+Management/AWS-Budgets_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AWS+Cost+Management/AWS-Budgets_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Cost-and-Usage-Report",
    "分野": "AWS Cost Management",
    "画像パス": "AWS+Cost+Management/AWS-Cost-and-Usage-Report_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AWS+Cost+Management/AWS-Cost-and-Usage-Report_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Cost-Explorer",
    "分野": "AWS Cost Management",
    "画像パス": "AWS+Cost+Management/AWS-Cost-Explorer_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AWS+Cost+Management/AWS-Cost-Explorer_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Cost-Management",
    "分野": "AWS Cost Management",
    "画像パス": "AWS+Cost+Management/AWS-Cost-Management_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AWS+Cost+Management/AWS-Cost-Management_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Reserved-Instance-Reporting",
    "分野": "AWS Cost Management",
    "画像パス": "AWS+Cost+Management/Reserved-Instance-Reporting_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AWS+Cost+Management/Reserved-Instance-Reporting_dark-bg@4x.png",
    "説明": "リザーブドインスタンスの使用状況や普通に送るリザーブドインスタンスの 割合などをできる 画面",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Managed-Blockchain",
    "分野": "Blockchain",
    "画像パス": "Blockchain/Amazon-Managed-Blockchain_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Blockchain/Amazon-Managed-Blockchain_dark-bg@4x.png",
    "説明": "ブロックチェーンネットワークの基盤を構築できる。OSSのHyperledger かEthereumから選べる。Ethereumはまだリリースされてない",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Quantum-Ledger-Database-QLDB",
    "分野": "Blockchain",
    "画像パス": "Blockchain/Amazon-Quantum-Ledger-Database-QLDB_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Blockchain/Amazon-Quantum-Ledger-Database-QLDB_dark-bg@4x.png",
    "説明": "トランザクションをジャーナルに登録することがLedge台帳の情報を変更できる方法である。データを直接変更することができない。\nブロックチェーンと違い、その情報が分散環境にあるのではなく、中央集権的である。\n",
    "ソート": ""
  },
  {
    "製品名": "Blockchain",
    "分野": "Blockchain",
    "画像パス": "Blockchain/Blockchain_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Blockchain/Blockchain_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Alexa-For-Business",
    "分野": "Business Applications",
    "画像パス": "Business+Applications/Alexa-For-Business_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Business+Applications/Alexa-For-Business_dark-bg@4x.png",
    "説明": "Room idの登録をして使うビジネスアレクサ。日本語未対応。\n会議室空いているかどうか、行ってみたら空いていないとか無くせる？\n\n",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Chime",
    "分野": "Business Applications",
    "画像パス": "Business+Applications/Amazon-Chime_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Business+Applications/Amazon-Chime_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-WorkMail",
    "分野": "Business Applications",
    "画像パス": "Business+Applications/Amazon-WorkMail_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Business+Applications/Amazon-WorkMail_dark-bg@4x.png",
    "説明": "社員用メールアドレスの発行、ブラウザでメールクライアント",
    "ソート": ""
  },
  {
    "製品名": "Business-Applications",
    "分野": "Business Applications",
    "画像パス": "Business+Applications/Business-Applications_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Business+Applications/Business-Applications_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-EC2-Auto-Scaling",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-EC2-Auto-Scaling_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2-Auto-Scaling_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-EC2-Container-Registry",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-EC2-Container-Registry_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2-Container-Registry_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-EC2-Container-Registry_Image",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-EC2-Container-Registry_Image_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2-Container-Registry_Image_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-EC2-Container-Registry_Registry",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-EC2-Container-Registry_Registry_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2-Container-Registry_Registry_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-EC2_AMI",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-EC2_AMI_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2_AMI_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-EC2_Auto-Scaling",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-EC2_Auto-Scaling_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2_Auto-Scaling_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-EC2",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-EC2_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-EC2_Elastic-IP-Address",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-EC2_Elastic-IP-Address_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2_Elastic-IP-Address_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-EC2_Spot-Instance",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-EC2_Spot-Instance_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2_Spot-Instance_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Elastic-Container-Service-for-Kubernetes",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-Elastic-Container-Service-for-Kubernetes_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Elastic-Container-Service-for-Kubernetes_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Elastic-Container-Service_Container1",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-Elastic-Container-Service_Container1_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Elastic-Container-Service_Container1_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Elastic-Container-Service_Container2",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-Elastic-Container-Service_Container2_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Elastic-Container-Service_Container2_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Elastic-Container-Service_Container3",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-Elastic-Container-Service_Container3_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Elastic-Container-Service_Container3_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Elastic-Container-Service",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-Elastic-Container-Service_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Elastic-Container-Service_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Elastic-Container-Service_dark-bg_1@4x.png",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-Elastic-Container-Service_dark-bg_1@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Elastic-Container-Service_dark-bg_1@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Elastic-Container-Service_Service",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-Elastic-Container-Service_Service_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Elastic-Container-Service_Service_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Lightsail",
    "分野": "Compute",
    "画像パス": "Compute/Amazon-Lightsail_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Lightsail_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Batch",
    "分野": "Compute",
    "画像パス": "Compute/AWS-Batch_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Batch_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Elastic-Beanstalk_Application",
    "分野": "Compute",
    "画像パス": "Compute/AWS-Elastic-Beanstalk_Application_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Elastic-Beanstalk_Application_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Elastic-Beanstalk",
    "分野": "Compute",
    "画像パス": "Compute/AWS-Elastic-Beanstalk_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Elastic-Beanstalk_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Elastic-Beanstalk_Deployment",
    "分野": "Compute",
    "画像パス": "Compute/AWS-Elastic-Beanstalk_Deployment_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Elastic-Beanstalk_Deployment_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Fargate",
    "分野": "Compute",
    "画像パス": "Compute/AWS-Fargate_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Fargate_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Lambda",
    "分野": "Compute",
    "画像パス": "Compute/AWS-Lambda_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Lambda_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Lambda_Lambda-Function",
    "分野": "Compute",
    "画像パス": "Compute/AWS-Lambda_Lambda-Function_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Lambda_Lambda-Function_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Outposts",
    "分野": "Compute",
    "画像パス": "Compute/AWS-Outposts_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Outposts_dark-bg@4x.png",
    "説明": "AWSの各サービスをオンプレミスで稼働させれる。EC2が初め, RDS, ECS,EKS, Sagemaker,EMRがついか",
    "ソート": ""
  },
  {
    "製品名": "AWS-Serverless-Application-Repository",
    "分野": "Compute",
    "画像パス": "Compute/AWS-Serverless-Application-Repository_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Serverless-Application-Repository_dark-bg@4x.png",
    "説明": "lambdaをベースにSAMで作られたサーバレスアプリケーションを登録して、使い回すこと、他人に公開できる",
    "ソート": ""
  },
  {
    "製品名": "Compute",
    "分野": "Compute",
    "画像パス": "Compute/Compute_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Compute_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "VMware-Cloud-On-AWS",
    "分野": "Compute",
    "画像パス": "Compute/VMware-Cloud-On-AWS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/VMware-Cloud-On-AWS_dark-bg@4x.png",
    "説明": "AWSとVMWareがタッグを組んで、AWSのハードウェア環境にVMWare社がきちんとサポートするVMWareホスティングサービス。VMware Cloudの管理画面から作成場所をAWSにして作成できる",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Connect",
    "分野": "Customer Engagement",
    "画像パス": "Customer+Engagement/Amazon-Connect_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Customer+Engagement/Amazon-Connect_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Pinpoint",
    "分野": "Customer Engagement",
    "画像パス": "Customer+Engagement/Amazon-Pinpoint_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Customer+Engagement/Amazon-Pinpoint_dark-bg@4x.png",
    "説明": "通知をセグメント化して送れる。メールとしても送れる。開封したかなどの情報を取得できる。",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Simple-Email-Service-SES",
    "分野": "Customer Engagement",
    "画像パス": "Customer+Engagement/Amazon-Simple-Email-Service-SES_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Customer+Engagement/Amazon-Simple-Email-Service-SES_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Simple-Email-Service-SES_Email",
    "分野": "Customer Engagement",
    "画像パス": "Customer+Engagement/Amazon-Simple-Email-Service-SES_Email_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Customer+Engagement/Amazon-Simple-Email-Service-SES_Email_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Customer-Engagement",
    "分野": "Customer Engagement",
    "画像パス": "Customer+Engagement/Customer-Engagement_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Customer+Engagement/Customer-Engagement_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Aurora",
    "分野": "Database",
    "画像パス": "Database/Amazon-Aurora_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-Aurora_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-DynamoDB_Attributes",
    "分野": "Database",
    "画像パス": "Database/Amazon-DynamoDB_Attributes_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-DynamoDB_Attributes_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-DynamoDB_Attribute",
    "分野": "Database",
    "画像パス": "Database/Amazon-DynamoDB_Attribute_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-DynamoDB_Attribute_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-DynamoDB",
    "分野": "Database",
    "画像パス": "Database/Amazon-DynamoDB_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-DynamoDB_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-DynamoDB_Global-Secondary-Index",
    "分野": "Database",
    "画像パス": "Database/Amazon-DynamoDB_Global-Secondary-Index_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-DynamoDB_Global-Secondary-Index_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-DynamoDB_Items",
    "分野": "Database",
    "画像パス": "Database/Amazon-DynamoDB_Items_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-DynamoDB_Items_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-DynamoDB_Item",
    "分野": "Database",
    "画像パス": "Database/Amazon-DynamoDB_Item_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-DynamoDB_Item_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-DynamoDB_Table",
    "分野": "Database",
    "画像パス": "Database/Amazon-DynamoDB_Table_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-DynamoDB_Table_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-ElastiCache_Cache-Node",
    "分野": "Database",
    "画像パス": "Database/Amazon-ElastiCache_Cache-Node_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-ElastiCache_Cache-Node_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-ElastiCache",
    "分野": "Database",
    "画像パス": "Database/Amazon-ElastiCache_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-ElastiCache_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-ElastiCache_For-Memcached",
    "分野": "Database",
    "画像パス": "Database/Amazon-ElastiCache_For-Memcached_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-ElastiCache_For-Memcached_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-ElastiCache_For-Redis",
    "分野": "Database",
    "画像パス": "Database/Amazon-ElastiCache_For-Redis_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-ElastiCache_For-Redis_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Neptune",
    "分野": "Database",
    "画像パス": "Database/Amazon-Neptune_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-Neptune_dark-bg@4x.png",
    "説明": "マネージドグラフ型データベース",
    "ソート": ""
  },
  {
    "製品名": "Amazon-RDS-on-VMware",
    "分野": "Database",
    "画像パス": "Database/Amazon-RDS-on-VMware_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-RDS-on-VMware_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-RDS",
    "分野": "Database",
    "画像パス": "Database/Amazon-RDS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-RDS_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Redshift",
    "分野": "Database",
    "画像パス": "Database/Amazon-Redshift_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-Redshift_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Redshift_Dense-Compute-Node",
    "分野": "Database",
    "画像パス": "Database/Amazon-Redshift_Dense-Compute-Node_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-Redshift_Dense-Compute-Node_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Redshift_Dense-Storage-Node",
    "分野": "Database",
    "画像パス": "Database/Amazon-Redshift_Dense-Storage-Node_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-Redshift_Dense-Storage-Node_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Timestream",
    "分野": "Database",
    "画像パス": "Database/Amazon-Timestream_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-Timestream_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Database-Migration-Service",
    "分野": "Database",
    "画像パス": "Database/AWS-Database-Migration-Service_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/AWS-Database-Migration-Service_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Database-Migration-Service_Database-Migration-Workflow",
    "分野": "Database",
    "画像パス": "Database/AWS-Database-Migration-Service_Database-Migration-Workflow_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/AWS-Database-Migration-Service_Database-Migration-Workflow_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Database",
    "分野": "Database",
    "画像パス": "Database/Database_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Database_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Cloud9",
    "分野": "Developer Tools",
    "画像パス": "Developer+Tools/AWS-Cloud9_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-Cloud9_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-CodeBuild",
    "分野": "Developer Tools",
    "画像パス": "Developer+Tools/AWS-CodeBuild_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-CodeBuild_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-CodeCommit",
    "分野": "Developer Tools",
    "画像パス": "Developer+Tools/AWS-CodeCommit_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-CodeCommit_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-CodeDeploy",
    "分野": "Developer Tools",
    "画像パス": "Developer+Tools/AWS-CodeDeploy_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-CodeDeploy_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-CodePipeline",
    "分野": "Developer Tools",
    "画像パス": "Developer+Tools/AWS-CodePipeline_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-CodePipeline_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-CodeStar",
    "分野": "Developer Tools",
    "画像パス": "Developer+Tools/AWS-CodeStar_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-CodeStar_dark-bg@4x.png",
    "説明": "AWS code系を全部統合させる感じのサービス。リポジトリ、CI、デプロイの管理を統合してできる",
    "ソート": ""
  },
  {
    "製品名": "AWS-Command-Line-Interface",
    "分野": "Developer Tools",
    "画像パス": "Developer+Tools/AWS-Command-Line-Interface_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-Command-Line-Interface_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Tools-And-SDKs",
    "分野": "Developer Tools",
    "画像パス": "Developer+Tools/AWS-Tools-And-SDKs_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-Tools-And-SDKs_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-X-Ray",
    "分野": "Developer Tools",
    "画像パス": "Developer+Tools/AWS-X-Ray_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-X-Ray_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Developer-Tools",
    "分野": "Developer Tools",
    "画像パス": "Developer+Tools/Developer-Tools_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/Developer-Tools_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Appstream-2.0",
    "分野": "End User Computing",
    "画像パス": "End+User+Computing/Amazon-Appstream-2.0_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/End+User+Computing/Amazon-Appstream-2.0_dark-bg@4x.png",
    "説明": "AWSのサーバー内にデスクトップアプリをインストールして、クライアントはブラウザから操作できる。ハンズオンとかで特定のアプリを使うときなど使える？",
    "ソート": ""
  },
  {
    "製品名": "Amazon-WorkDocs",
    "分野": "End User Computing",
    "画像パス": "End+User+Computing/Amazon-WorkDocs_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/End+User+Computing/Amazon-WorkDocs_dark-bg@4x.png",
    "説明": "AWSのDropbox。1ユーザー7USD",
    "ソート": ""
  },
  {
    "製品名": "Amazon-WorkLink",
    "分野": "End User Computing",
    "画像パス": "End+User+Computing/Amazon-WorkLink_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/End+User+Computing/Amazon-WorkLink_dark-bg@4x.png",
    "説明": "AWS内のプライベートなWEBに安全にアクセスできる。",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Workspaces",
    "分野": "End User Computing",
    "画像パス": "End+User+Computing/Amazon-Workspaces_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/End+User+Computing/Amazon-Workspaces_dark-bg@4x.png",
    "説明": "バーチャルデスクトップ環境をAWSに構築できるサービス",
    "ソート": ""
  },
  {
    "製品名": "End-User-Computing",
    "分野": "End User Computing",
    "画像パス": "End+User+Computing/End-User-Computing_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/End+User+Computing/End-User-Computing_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-GameLift",
    "分野": "Game Tech",
    "画像パス": "Game+Tech/Amazon-GameLift_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Game+Tech/Amazon-GameLift_dark-bg@4x.png",
    "説明": "Gameサーバーのインフラ部分のサービス。セッション数のモニタリングなどマネジメント部分がある程度ついている。中身はEC2のサーバーが起動している",
    "ソート": ""
  },
  {
    "製品名": "Game-Tech",
    "分野": "Game Tech",
    "画像パス": "Game+Tech/Game-Tech_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Game+Tech/Game-Tech_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-FreeRTOS",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/Amazon-FreeRTOS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/Amazon-FreeRTOS_dark-bg@4x.png",
    "説明": "AWSの作ったマイクロコンピューター用OS。オープンソースのFreeRTOSをベースに、AWSのIoT CoreやGreenglassに簡単に接続できるようライブラリが組み込まれている",
    "ソート": ""
  },
  {
    "製品名": "AWS-IoT-1-Click",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-1-Click_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-1-Click_dark-bg@4x.png",
    "説明": "1-clickで指定のラムダを動かせる物理ボタン",
    "ソート": ""
  },
  {
    "製品名": "AWS-IoT-Analytics_Channel",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Analytics_Channel_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Analytics_Channel_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-IoT-Analytics",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Analytics_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Analytics_dark-bg@4x.png",
    "説明": "IoT Coreで取り込まれたデータ、さらにはS3やDynamoDBからもデータを取り込み、処理をしてデータの保存ができる。分析や可視化の役割も担う。Kinesis Analyticsはめっちゃリアルタイム性の高い分析に用いるが、IoT Analyticsの方は分単位とかでの分析に用いられる",
    "ソート": ""
  },
  {
    "製品名": "AWS-IoT-Analytics_Data-Set",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Analytics_Data-Set_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Analytics_Data-Set_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-IoT-Analytics_Data-Store",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Analytics_Data-Store_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Analytics_Data-Store_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-IoT-Analytics_Notebook",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Analytics_Notebook_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Analytics_Notebook_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-IoT-Analytics_Pipeline",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Analytics_Pipeline_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Analytics_Pipeline_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-IoT-Button",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Button_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Button_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-IoT-Core",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Core_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Core_dark-bg@4x.png",
    "説明": "IoTデバイスの管理、証明書の発行など、IoTの基礎になるサービス",
    "ソート": ""
  },
  {
    "製品名": "AWS-IoT-Device-Defender",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Device-Defender_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Device-Defender_dark-bg@4x.png",
    "説明": "多数のデバイスをリモートでテストできるサービス",
    "ソート": ""
  },
  {
    "製品名": "AWS-IoT-Events",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Events_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Events_dark-bg@4x.png",
    "説明": "IoTデバイスでなにか変化を検出した際に、なにかActionを起こせる",
    "ソート": ""
  },
  {
    "製品名": "AWS-IoT-Greengrass_Connector",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Greengrass_Connector_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Greengrass_Connector_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-IoT-Greengrass",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Greengrass_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Greengrass_dark-bg@4x.png",
    "説明": "IoTの各機能をクラウドからエッジで行う事ができる。ここでいうエッジとはAWSのクラウドのデバイスをつなぐ、拠点サーバみたいなものである。UbunteなどにGreenglass Coreをインストールすることができて、そのサーバ内でLambdaや推論を行うことができる。つまり完全にインターネットに繋がっていない環境でもクラウドを動かせる",
    "ソート": ""
  },
  {
    "製品名": "AWS-IoT-SiteWise",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-SiteWise_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-SiteWise_dark-bg@4x.png",
    "説明": "工場の各センサーのデータをクラウドにアップするために、ゲートウェイサービスを展開し、データのクラウド上での可視化を可能にする。Snowball Edgeが動作環境としてあるため、借りればそれをゲートウェイにできる",
    "ソート": ""
  },
  {
    "製品名": "AWS-IoT-Things-Graph",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/AWS-IoT-Things-Graph_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Things-Graph_dark-bg@4x.png",
    "説明": "IoTのデータを入り口として各WEBサービス間連携のピタゴラスイッチをGUIで組める",
    "ソート": ""
  },
  {
    "製品名": "Internet-of-Things",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/Internet-of-Things_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/Internet-of-Things_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "IoT_Action",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Action_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Action_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Actuator",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Actuator_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Actuator_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Alexa-enabled-device",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Alexa-enabled-device_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Alexa-enabled-device_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Alexa-skill",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Alexa-skill_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Alexa-skill_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Alexa-voice-service",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Alexa-voice-service_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Alexa-voice-service_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Bank",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Bank_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Bank_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Bicycle",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Bicycle_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Bicycle_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Camera",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Camera_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Camera_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Cart",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Cart_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Cart_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Car",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Car_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Car_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Certificate-manager",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Certificate-manager_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Certificate-manager_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Coffee-pot",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Coffee-pot_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Coffee-pot_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Desired-state",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Desired-state_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Desired-state_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Device-gateway",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Device-gateway_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Device-gateway_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Door-lock",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Door-lock_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Door-lock_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Echo",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Echo_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Echo_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Factory",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Factory_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Factory_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Fire-TV-stick",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Fire-TV-stick_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Fire-TV-stick_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Fire-TV",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Fire-TV_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Fire-TV_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Generic",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Generic_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Generic_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Hardware-board",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Hardware-board_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Hardware-board_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_House",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_House_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_House_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_HTTP-2-protocol",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_HTTP-2-protocol_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_HTTP-2-protocol_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_HTTP-protocol",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_HTTP-protocol_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_HTTP-protocol_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Lambda-function",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Lambda-function_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Lambda-function_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Lightbulb",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Lightbulb_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Lightbulb_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Medical-emergency",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Medical-emergency_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Medical-emergency_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_MQTT-protocol",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_MQTT-protocol_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_MQTT-protocol_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Over-the-air-update",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Over-the-air-update_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Over-the-air-update_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Police-emergency",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Police-emergency_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Police-emergency_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Policy",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Policy_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Policy_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Reported-state",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Reported-state_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Reported-state_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Rule",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Rule_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Rule_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Sensor",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Sensor_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Sensor_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Servo",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Servo_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Servo_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Shadow",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Shadow_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Shadow_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Simulator",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Simulator_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Simulator_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Thermostat",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Thermostat_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Thermostat_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Topic",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Topic_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Topic_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Travel",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Travel_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Travel_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Utility",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Utility_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Utility_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "IoT_Windfarm",
    "分野": "Internet of Things",
    "画像パス": "Internet+of+Things/IoT_Windfarm_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/IoT_Windfarm_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Comprehend",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Comprehend_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Comprehend_dark-bg@4x.png",
    "説明": "自然言語処理。\nテキストを投げると品詞分解や感情分解ができるAPI。日本語対応はまだ",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Elastic-Inference",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Elastic-Inference_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Elastic-Inference_dark-bg@4x.png",
    "説明": "EC2やSageMakerのインスタンスにGPU増しをattachできる機能",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Forecast",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Forecast_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Forecast_dark-bg@4x.png",
    "説明": "時系列データの予測をしてくれるサービス",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Lex",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Lex_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Lex_dark-bg@4x.png",
    "説明": "対話式インターフェースのチャットボットを構築できるサービス。Slack、Facebook、Kik、Twilio SMSが対応している",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Personalize",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Personalize_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Personalize_dark-bg@4x.png",
    "説明": "Amazon.comの経験を活かした、いい感じにパーソナライズをやってくれるマネージド・サービス",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Polly",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Polly_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Polly_dark-bg@4x.png",
    "説明": "テキストを音声で読み上げるサービス",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Rekognition",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Rekognition_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Rekognition_dark-bg@4x.png",
    "説明": "画像認識サービス。画像が何なのかのタグづけや、文字の認識ができる",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Rekognition_Image",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Rekognition_Image_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Rekognition_Image_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Rekognition_Video",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Rekognition_Video_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Rekognition_Video_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-SageMaker-Ground-Truth",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-SageMaker-Ground-Truth_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-SageMaker-Ground-Truth_dark-bg@4x.png",
    "説明": "教師ありデータの機械学習をする際のラベル付けサービス",
    "ソート": ""
  },
  {
    "製品名": "Amazon-SageMaker",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-SageMaker_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-SageMaker_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-SageMaker_Model",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-SageMaker_Model_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-SageMaker_Model_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-SageMaker_Notebook",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-SageMaker_Notebook_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-SageMaker_Notebook_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-SageMaker_Train",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-SageMaker_Train_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-SageMaker_Train_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Textract",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Textract_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Textract_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Transcribe",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Transcribe_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Transcribe_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Translate",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Amazon-Translate_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Translate_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Apache-MXNet-on-AWS",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Apache-MXNet-on-AWS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Apache-MXNet-on-AWS_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Deep-Learning-AMIs",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/AWS-Deep-Learning-AMIs_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/AWS-Deep-Learning-AMIs_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Deep-Learning-Containers",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/AWS-Deep-Learning-Containers_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/AWS-Deep-Learning-Containers_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-DeepLens",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/AWS-DeepLens_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/AWS-DeepLens_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-DeepRacer",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/AWS-DeepRacer_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/AWS-DeepRacer_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Machine-Learning",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/Machine-Learning_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Machine-Learning_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "TensorFlow-on-AWS",
    "分野": "Machine Learning",
    "画像パス": "Machine+Learning/TensorFlow-on-AWS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/TensorFlow-on-AWS_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-CloudWatch_Alarm",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/Amazon-CloudWatch_Alarm_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/Amazon-CloudWatch_Alarm_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-CloudWatch",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/Amazon-CloudWatch_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/Amazon-CloudWatch_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-CloudWatch_Event-Event-Based",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/Amazon-CloudWatch_Event-Event-Based_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/Amazon-CloudWatch_Event-Event-Based_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-CloudWatch_Event-Time-Based",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/Amazon-CloudWatch_Event-Time-Based_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/Amazon-CloudWatch_Event-Time-Based_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-CloudWatch_Rule",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/Amazon-CloudWatch_Rule_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/Amazon-CloudWatch_Rule_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Auto-Scaling",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Auto-Scaling_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Auto-Scaling_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-CloudFormation_Change-Set",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-CloudFormation_Change-Set_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-CloudFormation_Change-Set_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-CloudFormation",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-CloudFormation_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-CloudFormation_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-CloudFormation_Stack",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-CloudFormation_Stack_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-CloudFormation_Stack_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-CloudFormation_Template",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-CloudFormation_Template_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-CloudFormation_Template_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-CloudTrail",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-CloudTrail_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-CloudTrail_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Command-Line-Interface",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Command-Line-Interface_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Command-Line-Interface_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Config",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Config_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Config_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Control-Tower",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Control-Tower_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Control-Tower_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-License-Manager",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-License-Manager_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-License-Manager_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Managed-Services",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Managed-Services_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Managed-Services_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Management-Console",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Management-Console_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Management-Console_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-OpsWorks_Apps",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-OpsWorks_Apps_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-OpsWorks_Apps_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-OpsWorks",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-OpsWorks_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-OpsWorks_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-OpsWorks_Deployments",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-OpsWorks_Deployments_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-OpsWorks_Deployments_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-OpsWorks_Instances",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-OpsWorks_Instances_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-OpsWorks_Instances_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-OpsWorks_Layers",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-OpsWorks_Layers_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-OpsWorks_Layers_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-OpsWorks_Monitoring",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-OpsWorks_Monitoring_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-OpsWorks_Monitoring_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-OpsWorks_Permissions",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-OpsWorks_Permissions_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-OpsWorks_Permissions_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-OpsWorks_Resources",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-OpsWorks_Resources_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-OpsWorks_Resources_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-OpsWorks_Stack2_Automation",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-OpsWorks_Stack2_Automation_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-OpsWorks_Stack2_Automation_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Organizations_Account",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Organizations_Account_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Organizations_Account_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Organizations",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Organizations_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Organizations_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Organizations_Organizational-Unit-OU",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Organizations_Organizational-Unit-OU_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Organizations_Organizational-Unit-OU_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Personal-Health-Dashboard",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Personal-Health-Dashboard_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Personal-Health-Dashboard_dark-bg@4x.png",
    "説明": "現在利用中のサービスの状態を知ることができるダッシュボード",
    "ソート": ""
  },
  {
    "製品名": "AWS-Service-Catalog",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Service-Catalog_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Service-Catalog_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Systems-Manager_Automation",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Systems-Manager_Automation_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Systems-Manager_Automation_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Systems-Manager",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Systems-Manager_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Systems-Manager_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Systems-Manager_Documents",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Systems-Manager_Documents_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Systems-Manager_Documents_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Systems-Manager_Inventory-Windows",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Systems-Manager_Inventory-Windows_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Systems-Manager_Inventory-Windows_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Systems-Manager_Maintenance-Windows",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Systems-Manager_Maintenance-Windows_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Systems-Manager_Maintenance-Windows_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Systems-Manager_Parameter-Store",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Systems-Manager_Parameter-Store_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Systems-Manager_Parameter-Store_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Systems-Manager_Patch-Manager",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Systems-Manager_Patch-Manager_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Systems-Manager_Patch-Manager_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Systems-Manager_Run-Command",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Systems-Manager_Run-Command_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Systems-Manager_Run-Command_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Systems-Manager_State-Manager",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Systems-Manager_State-Manager_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Systems-Manager_State-Manager_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Trusted-Advisor_Checklist-Cost",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Trusted-Advisor_Checklist-Cost_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Trusted-Advisor_Checklist-Cost_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Trusted-Advisor_Checklist-Fault-Tolerant",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Trusted-Advisor_Checklist-Fault-Tolerant_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Trusted-Advisor_Checklist-Fault-Tolerant_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Trusted-Advisor_Checklist-Performance",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Trusted-Advisor_Checklist-Performance_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Trusted-Advisor_Checklist-Performance_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Trusted-Advisor_Checklist-Security",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Trusted-Advisor_Checklist-Security_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Trusted-Advisor_Checklist-Security_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Trusted-Advisor_Checklist",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Trusted-Advisor_Checklist_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Trusted-Advisor_Checklist_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Trusted-Advisor",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Trusted-Advisor_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Trusted-Advisor_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Well-Architected-Tool",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/AWS-Well-Architected-Tool_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Well-Architected-Tool_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Management-and-Governance",
    "分野": "Management & Governance",
    "画像パス": "Management+&+Governance/Management-and-Governance_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/Management-and-Governance_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Elastic-Transcoder",
    "分野": "Media Services",
    "画像パス": "Media+Services/Amazon-Elastic-Transcoder_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Media+Services/Amazon-Elastic-Transcoder_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Kinesis-Video-Streams",
    "分野": "Media Services",
    "画像パス": "Media+Services/Amazon-Kinesis-Video-Streams_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Media+Services/Amazon-Kinesis-Video-Streams_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Elemental-MediaConnect",
    "分野": "Media Services",
    "画像パス": "Media+Services/AWS-Elemental-MediaConnect_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Media+Services/AWS-Elemental-MediaConnect_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Elemental-MediaLive",
    "分野": "Media Services",
    "画像パス": "Media+Services/AWS-Elemental-MediaLive_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Media+Services/AWS-Elemental-MediaLive_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Media-Services",
    "分野": "Media Services",
    "画像パス": "Media+Services/Media-Services_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Media+Services/Media-Services_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Application-Discovery-Service",
    "分野": "Migration & Transfer",
    "画像パス": "Migration+&+Transfer/AWS-Application-Discovery-Service_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Application-Discovery-Service_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-DataSync_Agent",
    "分野": "Migration & Transfer",
    "画像パス": "Migration+&+Transfer/AWS-DataSync_Agent_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-DataSync_Agent_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-DataSync",
    "分野": "Migration & Transfer",
    "画像パス": "Migration+&+Transfer/AWS-DataSync_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-DataSync_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Migration-Hub",
    "分野": "Migration & Transfer",
    "画像パス": "Migration+&+Transfer/AWS-Migration-Hub_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Migration-Hub_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Server-Migration-Service",
    "分野": "Migration & Transfer",
    "画像パス": "Migration+&+Transfer/AWS-Server-Migration-Service_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Server-Migration-Service_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Snowball-Edge",
    "分野": "Migration & Transfer",
    "画像パス": "Migration+&+Transfer/AWS-Snowball-Edge_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Snowball-Edge_dark-bg@4x.png",
    "説明": "snowball登場の一年後に出た上位品。\nネットワークインターフェースや、100TBの容量など向上し、ソフトウェアもその筐体内でS3やLambdaも動く。AWSに送り返す前にデータを加工して投入できる。\n10日間使って3万円",
    "ソート": ""
  },
  {
    "製品名": "AWS-Snowball",
    "分野": "Migration & Transfer",
    "画像パス": "Migration+&+Transfer/AWS-Snowball_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Snowball_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Snowmobile",
    "分野": "Migration & Transfer",
    "画像パス": "Migration+&+Transfer/AWS-Snowmobile_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Snowmobile_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Transfer-for-SFTP",
    "分野": "Migration & Transfer",
    "画像パス": "Migration+&+Transfer/AWS-Transfer-for-SFTP_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Transfer-for-SFTP_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Migration-and-Transfer",
    "分野": "Migration & Transfer",
    "画像パス": "Migration+&+Transfer/Migration-and-Transfer_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/Migration-and-Transfer_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-API-Gateway",
    "分野": "Mobile",
    "画像パス": "Mobile/Amazon-API-Gateway_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/Amazon-API-Gateway_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-API-Gateway_Endpoint",
    "分野": "Mobile",
    "画像パス": "Mobile/Amazon-API-Gateway_Endpoint_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/Amazon-API-Gateway_Endpoint_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Pinpoint",
    "分野": "Mobile",
    "画像パス": "Mobile/Amazon-Pinpoint_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/Amazon-Pinpoint_dark-bg@4x.png",
    "説明": "セグメント分けて通知するやつ",
    "ソート": 0
  },
  {
    "製品名": "AWS-Amplify",
    "分野": "Mobile",
    "画像パス": "Mobile/AWS-Amplify_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/AWS-Amplify_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-AppSync",
    "分野": "Mobile",
    "画像パス": "Mobile/AWS-AppSync_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/AWS-AppSync_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Device-Farm",
    "分野": "Mobile",
    "画像パス": "Mobile/AWS-Device-Farm_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/AWS-Device-Farm_dark-bg@4x.png",
    "説明": "いろんな機種でテストするやつ",
    "ソート": ""
  },
  {
    "製品名": "Mobile",
    "分野": "Mobile",
    "画像パス": "Mobile/Mobile_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/Mobile_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-API-Gateway",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-API-Gateway_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-API-Gateway_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-API-Gateway_Endpoint",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-API-Gateway_Endpoint_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-API-Gateway_Endpoint_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-CloudFront",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-CloudFront_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-CloudFront_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-CloudFront_Download-Distribution",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-CloudFront_Download-Distribution_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-CloudFront_Download-Distribution_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-CloudFront_Edge-Location",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-CloudFront_Edge-Location_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-CloudFront_Edge-Location_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-CloudFront_Streaming-Distribution",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-CloudFront_Streaming-Distribution_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-CloudFront_Streaming-Distribution_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Route-53",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-Route-53_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-Route-53_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Route-53_Hosted-Zone",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-Route-53_Hosted-Zone_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-Route-53_Hosted-Zone_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Route-53_Route-Table",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-Route-53_Route-Table_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-Route-53_Route-Table_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_Customer-Gateway",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_Customer-Gateway_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_Customer-Gateway_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-VPC_Elastic-Network-Adapter",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_Elastic-Network-Adapter_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_Elastic-Network-Adapter_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_Elastic-Network-Interface",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_Elastic-Network-Interface_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_Elastic-Network-Interface_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_Endpoints",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_Endpoints_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_Endpoints_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_Flow-Logs",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_Flow-Logs_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_Flow-Logs_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_Internet-Gateway",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_Internet-Gateway_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_Internet-Gateway_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_NAT-Gateway",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_NAT-Gateway_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_NAT-Gateway_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_Network-Access-Control-List",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_Network-Access-Control-List_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_Network-Access-Control-List_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_Peering",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_Peering_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_Peering_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_Router",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_Router_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_Router_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_VPN-Connection",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_VPN-Connection_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_VPN-Connection_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-VPC_VPN-Gateway",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Amazon-VPC_VPN-Gateway_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_VPN-Gateway_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-App-Mesh",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/AWS-App-Mesh_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-App-Mesh_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Client-VPN",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/AWS-Client-VPN_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Client-VPN_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Cloud-Map",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/AWS-Cloud-Map_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Cloud-Map_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Direct-Connect",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/AWS-Direct-Connect_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Direct-Connect_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Global-Accelerator",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/AWS-Global-Accelerator_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Global-Accelerator_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-PrivateLink",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/AWS-PrivateLink_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-PrivateLink_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Site-to-Site-VPN",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/AWS-Site-to-Site-VPN_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Site-to-Site-VPN_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Transit-Gateway",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/AWS-Transit-Gateway_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Transit-Gateway_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Elastic-Load-Balancing-ELB_Application-load-balancer",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Elastic-Load-Balancing-ELB_Application-load-balancer_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Elastic-Load-Balancing-ELB_Application-load-balancer_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Elastic-Load-Balancing-ELB_Network-load-balancer",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Elastic-Load-Balancing-ELB_Network-load-balancer_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Elastic-Load-Balancing-ELB_Network-load-balancer_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Elastic-Load-Balancing_Classic-load-balancer",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Elastic-Load-Balancing_Classic-load-balancer_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Elastic-Load-Balancing_Classic-load-balancer_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Elastic-Load-Balancing",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Elastic-Load-Balancing_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Elastic-Load-Balancing_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Networking-and-Content-Delivery",
    "分野": "Networking & Content Delivery",
    "画像パス": "Networking+&+Content+Delivery/Networking-and-Content-Delivery_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Networking-and-Content-Delivery_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-RoboMaker_Cloud-Extension-ROS",
    "分野": "Robotics",
    "画像パス": "Robotics/AWS-RoboMaker_Cloud-Extension-ROS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Robotics/AWS-RoboMaker_Cloud-Extension-ROS_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-RoboMaker",
    "分野": "Robotics",
    "画像パス": "Robotics/AWS-RoboMaker_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Robotics/AWS-RoboMaker_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-RoboMaker_Development-Environment",
    "分野": "Robotics",
    "画像パス": "Robotics/AWS-RoboMaker_Development-Environment_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Robotics/AWS-RoboMaker_Development-Environment_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-RoboMaker_Fleet-Management",
    "分野": "Robotics",
    "画像パス": "Robotics/AWS-RoboMaker_Fleet-Management_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Robotics/AWS-RoboMaker_Fleet-Management_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-RoboMaker_Simulation",
    "分野": "Robotics",
    "画像パス": "Robotics/AWS-RoboMaker_Simulation_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Robotics/AWS-RoboMaker_Simulation_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Robotics",
    "分野": "Robotics",
    "画像パス": "Robotics/Robotics_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Robotics/Robotics_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Ground-Station",
    "分野": "Satellite",
    "画像パス": "Satellite/AWS-Ground-Station_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Satellite/AWS-Ground-Station_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Satellite",
    "分野": "Satellite",
    "画像パス": "Satellite/Satellite_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Satellite/Satellite_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Cloud-Directory",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/Amazon-Cloud-Directory_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-Cloud-Directory_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Cognito",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/Amazon-Cognito_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-Cognito_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-GuardDuty",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/Amazon-GuardDuty_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-GuardDuty_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Inspector_Agent",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/Amazon-Inspector_Agent_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-Inspector_Agent_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Inspector",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/Amazon-Inspector_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-Inspector_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Macie",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/Amazon-Macie_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-Macie_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Artifact",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Artifact_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Artifact_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Certificate-Manager_Certificate-Manager",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Certificate-Manager_Certificate-Manager_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Certificate-Manager_Certificate-Manager_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Certificate-Manager",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Certificate-Manager_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Certificate-Manager_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-CloudHSM",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-CloudHSM_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-CloudHSM_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Directory-Service",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Directory-Service_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Directory-Service_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Firewall-Manager",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Firewall-Manager_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Firewall-Manager_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Identity-and-Access-Management-IAM_Add-on",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Add-on_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Add-on_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Identity-and-Access-Management-IAM_AWS-STS-Alternate",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_AWS-STS-Alternate_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_AWS-STS-Alternate_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Identity-and-Access-Management-IAM_AWS-STS",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_AWS-STS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_AWS-STS_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Identity-and-Access-Management-IAM_Data-Encryption-Key",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Data-Encryption-Key_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Data-Encryption-Key_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Identity-and-Access-Management-IAM_Encrypted-Data",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Encrypted-Data_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Encrypted-Data_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Identity-and-Access-Management-IAM_Long-term-Security-Credential",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Long-term-Security-Credential_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Long-term-Security-Credential_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Identity-and-Access-Management-IAM_MFA-Token",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_MFA-Token_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_MFA-Token_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Identity-and-Access-Management-IAM_Permissions",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Permissions_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Permissions_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Identity-and-Access-Management-IAM_Role",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Role_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Role_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Identity-and-Access-Management-IAM_Temporary-Security-Credential",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Temporary-Security-Credential_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Identity-and-Access-Management-IAM_Temporary-Security-Credential_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Key-Management-Service",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Key-Management-Service_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Key-Management-Service_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Resource-Access-Manager",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Resource-Access-Manager_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Resource-Access-Manager_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Secrets-Manager",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Secrets-Manager_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Secrets-Manager_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Security-Hub",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Security-Hub_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Security-Hub_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Shield",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Shield_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Shield_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Shield_Shield-Advanced",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Shield_Shield-Advanced_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Shield_Shield-Advanced_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Single-Sign-On",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-Single-Sign-On_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Single-Sign-On_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-WAF",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-WAF_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-WAF_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-WAF_Filtering-rule",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/AWS-WAF_Filtering-rule_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-WAF_Filtering-rule_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Security-Identity-and-Compliance",
    "分野": "Security, Identity, & Compliance",
    "画像パス": "Security,+Identity,+&+Compliance/Security-Identity-and-Compliance_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Security-Identity-and-Compliance_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Elastic-Block-Store-EBS",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-Elastic-Block-Store-EBS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-Elastic-Block-Store-EBS_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Elastic-Block-Store-EBS_Snapshot",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-Elastic-Block-Store-EBS_Snapshot_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-Elastic-Block-Store-EBS_Snapshot_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Elastic-Block-Store-EBS_Volume",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-Elastic-Block-Store-EBS_Volume_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-Elastic-Block-Store-EBS_Volume_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Elastic-File-System_EFS",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-Elastic-File-System_EFS_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-Elastic-File-System_EFS_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-Elastic-File-System_EFS_File-system",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-Elastic-File-System_EFS_File-system_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-Elastic-File-System_EFS_File-system_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-FSx-for-Lustre",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-FSx-for-Lustre_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-FSx-for-Lustre_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-FSx-for-Windows-File-Server",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-FSx-for-Windows-File-Server_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-FSx-for-Windows-File-Server_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-FSx",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-FSx_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-FSx_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-S3-Glacier_Archive",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-S3-Glacier_Archive_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-S3-Glacier_Archive_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-S3-Glacier",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-S3-Glacier_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-S3-Glacier_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Amazon-S3-Glacier_Vault",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-S3-Glacier_Vault_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-S3-Glacier_Vault_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Simple-Storage-Service-S3_Bucket-with-Objects",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-Simple-Storage-Service-S3_Bucket-with-Objects_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-Simple-Storage-Service-S3_Bucket-with-Objects_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Simple-Storage-Service-S3_Bucket",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-Simple-Storage-Service-S3_Bucket_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-Simple-Storage-Service-S3_Bucket_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Amazon-Simple-Storage-Service-S3_Object",
    "分野": "Storage",
    "画像パス": "Storage/Amazon-Simple-Storage-Service-S3_Object_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-Simple-Storage-Service-S3_Object_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Backup",
    "分野": "Storage",
    "画像パス": "Storage/AWS-Backup_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Backup_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Snow-Family_Snowball-Import-Export",
    "分野": "Storage",
    "画像パス": "Storage/AWS-Snow-Family_Snowball-Import-Export_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Snow-Family_Snowball-Import-Export_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Snowball-Edge",
    "分野": "Storage",
    "画像パス": "Storage/AWS-Snowball-Edge_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Snowball-Edge_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Snowball",
    "分野": "Storage",
    "画像パス": "Storage/AWS-Snowball_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Snowball_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Snowmobile",
    "分野": "Storage",
    "画像パス": "Storage/AWS-Snowmobile_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Snowmobile_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Storage-Gateway_Cached-Volume",
    "分野": "Storage",
    "画像パス": "Storage/AWS-Storage-Gateway_Cached-Volume_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Storage-Gateway_Cached-Volume_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Storage-Gateway",
    "分野": "Storage",
    "画像パス": "Storage/AWS-Storage-Gateway_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Storage-Gateway_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Storage-Gateway_Non-Cached-Volume",
    "分野": "Storage",
    "画像パス": "Storage/AWS-Storage-Gateway_Non-Cached-Volume_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Storage-Gateway_Non-Cached-Volume_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "AWS-Storage-Gateway_Virtual-Tape-Library",
    "分野": "Storage",
    "画像パス": "Storage/AWS-Storage-Gateway_Virtual-Tape-Library_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Storage-Gateway_Virtual-Tape-Library_dark-bg@4x.png",
    "説明": "",
    "ソート": 0
  },
  {
    "製品名": "Storage",
    "分野": "Storage",
    "画像パス": "Storage/Storage_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Storage_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Marketplace",
    "分野": "_General",
    "画像パス": "_General/AWS-Marketplace_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/AWS-Marketplace_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Client",
    "分野": "_General",
    "画像パス": "_General/Client_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Client_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Disk",
    "分野": "_General",
    "画像パス": "_General/Disk_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Disk_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Forums",
    "分野": "_General",
    "画像パス": "_General/Forums_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Forums_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "General",
    "分野": "_General",
    "画像パス": "_General/General_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/General_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Generic-database",
    "分野": "_General",
    "画像パス": "_General/Generic-database_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Generic-database_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Internet-alt1",
    "分野": "_General",
    "画像パス": "_General/Internet-alt1_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Internet-alt1_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Internet-alt2",
    "分野": "_General",
    "画像パス": "_General/Internet-alt2_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Internet-alt2_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Internet-gateway",
    "分野": "_General",
    "画像パス": "_General/Internet-gateway_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Internet-gateway_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Mobile-client",
    "分野": "_General",
    "画像パス": "_General/Mobile-client_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Mobile-client_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Multimedia",
    "分野": "_General",
    "画像パス": "_General/Multimedia_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Multimedia_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Office-building",
    "分野": "_General",
    "画像パス": "_General/Office-building_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Office-building_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "SAML-token",
    "分野": "_General",
    "画像パス": "_General/SAML-token_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/SAML-token_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "SDK",
    "分野": "_General",
    "画像パス": "_General/SDK_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/SDK_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "SSL-padlock",
    "分野": "_General",
    "画像パス": "_General/SSL-padlock_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/SSL-padlock_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Tape-storage",
    "分野": "_General",
    "画像パス": "_General/Tape-storage_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Tape-storage_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Toolkit",
    "分野": "_General",
    "画像パス": "_General/Toolkit_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Toolkit_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Traditional-server",
    "分野": "_General",
    "画像パス": "_General/Traditional-server_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Traditional-server_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Users",
    "分野": "_General",
    "画像パス": "_General/Users_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/Users_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "User",
    "分野": "_General",
    "画像パス": "_General/User_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_General/User_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Auto-Scaling",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/Auto-Scaling_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/Auto-Scaling_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Cloud-alt",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/AWS-Cloud-alt_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/AWS-Cloud-alt_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Cloud",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/AWS-Cloud_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/AWS-Cloud_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "AWS-Step-Function",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/AWS-Step-Function_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/AWS-Step-Function_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Corporate-data-center",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/Corporate-data-center_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/Corporate-data-center_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "EC2-instance-contents",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/EC2-instance-contents_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/EC2-instance-contents_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Elastic-Beanstalk-container",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/Elastic-Beanstalk-container_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/Elastic-Beanstalk-container_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Region",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/Region_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/Region_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Server-contents",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/Server-contents_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/Server-contents_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Spot-Fleet",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/Spot-Fleet_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/Spot-Fleet_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "Virtual-private-cloud-VPC",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/Virtual-private-cloud-VPC_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/Virtual-private-cloud-VPC_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "VPC-subnet-private",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/VPC-subnet-private_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/VPC-subnet-private_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  },
  {
    "製品名": "VPC-subnet-public",
    "分野": "_Group Icons",
    "画像パス": "_Group+Icons/VPC-subnet-public_dark-bg@4x.png",
    "ダミー": "",
    "リアル画像パス": "https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/_Group+Icons/VPC-subnet-public_dark-bg@4x.png",
    "説明": "",
    "ソート": ""
  }
];
