#!/usr/bin/env bash

docker run --rm -v $(pwd):/task --workdir=/task pin-image-generator ts-node src/index.ts
