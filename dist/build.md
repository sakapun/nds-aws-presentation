
# AR-VR

![AR-VR](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AR+&+VR/AR-VR_dark-bg@4x.png)



---------------


# Amazon-Sumerian

![Amazon-Sumerian](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AR+&+VR/Amazon-Sumerian_dark-bg@4x.png)

AR 空間で3 D モデルをお絵かきしたりできるツール
Unity をブラウザで使えるようなそんな感覚と言っていいんだろうか

---------------


# AWS-Cost-Management

![AWS-Cost-Management](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AWS+Cost+Management/AWS-Cost-Management_dark-bg@4x.png)



---------------


# AWS-Budgets

![AWS-Budgets](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AWS+Cost+Management/AWS-Budgets_dark-bg@4x.png)

予算管理。
しきい値を超えるとアラートを飛ばせる

---------------


# AWS-Cost-and-Usage-Report

![AWS-Cost-and-Usage-Report](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AWS+Cost+Management/AWS-Cost-and-Usage-Report_dark-bg@4x.png)

請求金額を確認する画面

---------------


# AWS-Cost-Explorer

![AWS-Cost-Explorer](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AWS+Cost+Management/AWS-Cost-Explorer_dark-bg@4x.png)

AWS コストと使用状況の経時的変化を可視化し、理解しやすい状態で管理することを可能にする

---------------


# Reserved-Instance-Reporting

![Reserved-Instance-Reporting](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/AWS+Cost+Management/Reserved-Instance-Reporting_dark-bg@4x.png)

リザーブドインスタンスの使用状況や、リザーブドインスタンスの割合などをできる画面

---------------


# Analytics

![Analytics](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Analytics_dark-bg@4x.png)



---------------


# Amazon-Athena

![Amazon-Athena](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Athena_dark-bg@4x.png)

S3の1ファイルをテーブルとして、SQLクエリで問い合わせできる

---------------


# Amazon-CloudSearch

![Amazon-CloudSearch](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-CloudSearch_dark-bg@4x.png)

投入済みのデータに対しての検索。
最近話を聞かない。
elastisearchと似ている

---------------


# Amazon-Elasticsearch-Service

![Amazon-Elasticsearch-Service](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Elasticsearch-Service_dark-bg@4x.png)

データに対して検索する。
勝手に色々できる。インスタンスが必要。

---------------


# Amazon-EMR

![Amazon-EMR](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-EMR_dark-bg@4x.png)

分散処理環境を利用することができる

---------------


# Amazon-Kinesis-Data-Analytics

![Amazon-Kinesis-Data-Analytics](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Kinesis-Data-Analytics_dark-bg@4x.png)

ストリームに対してクエリを投げられる

---------------


# Amazon-Kinesis-Data-Firehose

![Amazon-Kinesis-Data-Firehose](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Kinesis-Data-Firehose_dark-bg@4x.png)

ストリームデータを簡単に保存できるサービス

---------------


# Amazon-Kinesis-Data-Streams

![Amazon-Kinesis-Data-Streams](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Kinesis-Data-Streams_dark-bg@4x.png)

ストリームデータを保存する。
Firehoseより先に出たサービス。
早いがFirehoseよりちょっと設定が難しい

---------------


# Amazon-Kinesis-Video-Streams

![Amazon-Kinesis-Video-Streams](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Kinesis-Video-Streams_dark-bg@4x.png)

撮ったデータをすぐに分析かけれる。例えば動画撮って顔の検出とか

---------------


# Amazon-Kinesis

![Amazon-Kinesis](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Kinesis_dark-bg@4x.png)



---------------


# Amazon-Managed-Streaming-for-Kafka

![Amazon-Managed-Streaming-for-Kafka](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Managed-Streaming-for-Kafka_dark-bg@4x.png)

kinesis streamの部分の低レイテンシでいっぱい捌ける。オープンソースのapatch kafka

---------------


# Amazon-Quicksight

![Amazon-Quicksight](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Quicksight_dark-bg@4x.png)

分析ダッシュボードを簡単に作れるサービス

---------------


# Amazon-Redshift

![Amazon-Redshift](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/Amazon-Redshift_dark-bg@4x.png)

並列分散処理をする列志向データベース。
データウェアハウス。なのでデータの追加には向かず、バルク読込して解析に使う
16PBまでの容量もてる。
クラスタを常時上げておく必要がある

---------------


# AWS-Data-Pipeline

![AWS-Data-Pipeline](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/AWS-Data-Pipeline_dark-bg@4x.png)

データの移行サービス。具体的な事例として挙げられるのが定期的にRDSからRedshiftへのデータの注入が必要なとき。2015年

---------------


# AWS-Glue

![AWS-Glue](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/AWS-Glue_dark-bg@4x.png)



---------------


# AWS-Lake-Formation

![AWS-Lake-Formation](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Analytics/AWS-Lake-Formation_dark-bg@4x.png)

glueを中心とした、分析環境を構築できる。glueのラッパー
機能を使える人を制限する権限管理的な意味も含む
カタログ化とラベル付け、データの変換

---------------


# Application-Integration

![Application-Integration](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Application-Integration_dark-bg@4x.png)



---------------


# Amazon-AppSync

![Amazon-AppSync](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-AppSync_dark-bg@4x.png)

GraphQLのサーバを簡単に作れる

---------------


# Amazon-MQ

![Amazon-MQ](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-MQ_dark-bg@4x.png)

Apatch activeMQの フルマネージドサービス MQ はメッセージキューイングサービスの略

---------------


# Amazon-Simple-Notification-Service-SNS

![Amazon-Simple-Notification-Service-SNS](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-Simple-Notification-Service-SNS_dark-bg@4x.png)

マネージド型 pub/sub メッセージングサービス

---------------


# Amazon-Simple-Queue-Service-SQS

![Amazon-Simple-Queue-Service-SQS](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/Amazon-Simple-Queue-Service-SQS_dark-bg@4x.png)

マネージドジョブキューシステム

---------------


# AWS-Step-Functions

![AWS-Step-Functions](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Application+Integration/AWS-Step-Functions_dark-bg@4x.png)

Lambdaなどサーバレスな機能を順々につなげることができる

---------------


# Blockchain

![Blockchain](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Blockchain/Blockchain_dark-bg@4x.png)



---------------


# Amazon-Managed-Blockchain

![Amazon-Managed-Blockchain](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Blockchain/Amazon-Managed-Blockchain_dark-bg@4x.png)

ブロックチェーンネットワークの基盤を構築できる。
OSSのHyperledger かEthereumから選べる。
Ethereumはまだリリースされてない

---------------


# Amazon-Quantum-Ledger-Database-QLDB

![Amazon-Quantum-Ledger-Database-QLDB](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Blockchain/Amazon-Quantum-Ledger-Database-QLDB_dark-bg@4x.png)

台帳データベースサービス。
普通のDBと違って生データを直接変更することができなく、トランザクションの登録し変更をすべて合わせると現在の情報となる。
ブロックチェーンと違い、その情報が分散環境にあるのではなく、中央集権的である。

---------------


# Business-Applications

![Business-Applications](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Business+Applications/Business-Applications_dark-bg@4x.png)



---------------


# Alexa-For-Business

![Alexa-For-Business](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Business+Applications/Alexa-For-Business_dark-bg@4x.png)

Room idの登録をして使うビジネスアレクサ。日本語未対応。
会議室空いているかどうか、行ってみたら空いていないとか無くせるかも



---------------


# Amazon-Chime

![Amazon-Chime](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Business+Applications/Amazon-Chime_dark-bg@4x.png)

ミーティングシステム

---------------


# Amazon-WorkMail

![Amazon-WorkMail](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Business+Applications/Amazon-WorkMail_dark-bg@4x.png)

社員用メールアドレスの発行、ブラウザでメールクライアント

---------------


# Compute

![Compute](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Compute_dark-bg@4x.png)



---------------


# Amazon-EC2-Auto-Scaling

![Amazon-EC2-Auto-Scaling](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2-Auto-Scaling_dark-bg@4x.png)

閾値によってインスタンスを増減してくれる機能。

---------------


# Amazon-EC2-Container-Registry

![Amazon-EC2-Container-Registry](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2-Container-Registry_dark-bg@4x.png)

Docker Hubのようにコンテナを登録しておくサービス

---------------


# Amazon-EC2

![Amazon-EC2](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-EC2_dark-bg@4x.png)

ご存知クラウドサーバーを使いたいだけの時間稼働できる

---------------


# Amazon-Elastic-Container-Service-for-Kubernetes

![Amazon-Elastic-Container-Service-for-Kubernetes](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Elastic-Container-Service-for-Kubernetes_dark-bg@4x.png)

AWS上での、可用性が高く、スケーラブルで安全な Kubernetes サービス

---------------


# Amazon-Elastic-Container-Service

![Amazon-Elastic-Container-Service](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Elastic-Container-Service_dark-bg@4x.png)

EC2インスタンスを母艦としたコンテナ基盤サービス

---------------


# Amazon-Lightsail

![Amazon-Lightsail](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/Amazon-Lightsail_dark-bg@4x.png)

簡単にサーバーを借りれる、月額課金VPSサービス

---------------


# AWS-Batch

![AWS-Batch](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Batch_dark-bg@4x.png)

バッチジョブをマネージドな環境で実施できる。
Cronというよりは規模の大きな科学計算をやるとか

---------------


# AWS-Elastic-Beanstalk

![AWS-Elastic-Beanstalk](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Elastic-Beanstalk_dark-bg@4x.png)

アプリの単位で稼働のコントロールできるクラウドサービス

---------------


# AWS-Fargate

![AWS-Fargate](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Fargate_dark-bg@4x.png)

マネージドコンテナ基盤。
ECSと違い基盤にインスタンスを用意する必要がない。

---------------


# AWS-Lambda

![AWS-Lambda](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Lambda_dark-bg@4x.png)

サーバレスファンクションを実行できる

---------------


# AWS-Outposts

![AWS-Outposts](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Outposts_dark-bg@4x.png)

AWSの各サービスをオンプレミスで稼働させれる。EC2が初め, RDS, ECS,EKS, Sagemaker,EMRがついか

---------------


# AWS-Serverless-Application-Repository

![AWS-Serverless-Application-Repository](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/AWS-Serverless-Application-Repository_dark-bg@4x.png)

lambdaをベースにSAMで作られたサーバレスアプリケーションを登録して、使い回すこと、他人に公開できる

---------------


# VMware-Cloud-On-AWS

![VMware-Cloud-On-AWS](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Compute/VMware-Cloud-On-AWS_dark-bg@4x.png)

AWSとVMWareがタッグを組んで、AWSのハードウェア環境にVMWare社がきちんとサポートするVMWareホスティングサービス。VMware Cloudの管理画面から作成場所をAWSにして作成できる

---------------


# Customer-Engagement

![Customer-Engagement](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Customer+Engagement/Customer-Engagement_dark-bg@4x.png)



---------------


# Amazon-Connect

![Amazon-Connect](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Customer+Engagement/Amazon-Connect_dark-bg@4x.png)

簡単にAWS基盤でコールセンターを構築できる

---------------


# Amazon-Pinpoint

![Amazon-Pinpoint](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Customer+Engagement/Amazon-Pinpoint_dark-bg@4x.png)

通知をセグメント化して送れる。メールとしても送れる。開封したかなどの情報を取得できる。

---------------


# Amazon-Simple-Email-Service-SES

![Amazon-Simple-Email-Service-SES](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Customer+Engagement/Amazon-Simple-Email-Service-SES_dark-bg@4x.png)



---------------


# Database

![Database](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Database_dark-bg@4x.png)



---------------


# Amazon-Aurora

![Amazon-Aurora](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-Aurora_dark-bg@4x.png)



---------------


# Amazon-DynamoDB

![Amazon-DynamoDB](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-DynamoDB_dark-bg@4x.png)



---------------


# Amazon-ElastiCache

![Amazon-ElastiCache](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-ElastiCache_dark-bg@4x.png)



---------------


# Amazon-Neptune

![Amazon-Neptune](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-Neptune_dark-bg@4x.png)

マネージドグラフ型データベース

---------------


# Amazon-RDS-on-VMware

![Amazon-RDS-on-VMware](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-RDS-on-VMware_dark-bg@4x.png)



---------------


# Amazon-RDS

![Amazon-RDS](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-RDS_dark-bg@4x.png)



---------------


# Amazon-Redshift

![Amazon-Redshift](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-Redshift_dark-bg@4x.png)



---------------


# Amazon-Timestream

![Amazon-Timestream](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/Amazon-Timestream_dark-bg@4x.png)



---------------


# AWS-Database-Migration-Service

![AWS-Database-Migration-Service](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Database/AWS-Database-Migration-Service_dark-bg@4x.png)



---------------


# Developer-Tools

![Developer-Tools](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/Developer-Tools_dark-bg@4x.png)



---------------


# AWS-Cloud9

![AWS-Cloud9](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-Cloud9_dark-bg@4x.png)



---------------


# AWS-CodeBuild

![AWS-CodeBuild](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-CodeBuild_dark-bg@4x.png)



---------------


# AWS-CodeCommit

![AWS-CodeCommit](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-CodeCommit_dark-bg@4x.png)



---------------


# AWS-CodeDeploy

![AWS-CodeDeploy](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-CodeDeploy_dark-bg@4x.png)



---------------


# AWS-CodePipeline

![AWS-CodePipeline](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-CodePipeline_dark-bg@4x.png)



---------------


# AWS-CodeStar

![AWS-CodeStar](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-CodeStar_dark-bg@4x.png)

AWS code系を全部統合させる感じのサービス。リポジトリ、CI、デプロイの管理を統合してできる

---------------


# AWS-Command-Line-Interface

![AWS-Command-Line-Interface](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-Command-Line-Interface_dark-bg@4x.png)



---------------


# AWS-Tools-And-SDKs

![AWS-Tools-And-SDKs](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-Tools-And-SDKs_dark-bg@4x.png)



---------------


# AWS-X-Ray

![AWS-X-Ray](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Developer+Tools/AWS-X-Ray_dark-bg@4x.png)



---------------


# End-User-Computing

![End-User-Computing](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/End+User+Computing/End-User-Computing_dark-bg@4x.png)



---------------


# Amazon-Appstream-2.0

![Amazon-Appstream-2.0](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/End+User+Computing/Amazon-Appstream-2.0_dark-bg@4x.png)

AWSのサーバー内にデスクトップアプリをインストールして、クライアントはブラウザから操作できる。ハンズオンとかで特定のアプリを使うときなど使える？

---------------


# Amazon-WorkDocs

![Amazon-WorkDocs](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/End+User+Computing/Amazon-WorkDocs_dark-bg@4x.png)

AWSのDropbox。1ユーザー7USD

---------------


# Amazon-WorkLink

![Amazon-WorkLink](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/End+User+Computing/Amazon-WorkLink_dark-bg@4x.png)

AWS内のプライベートなWEBに安全にアクセスできる。

---------------


# Amazon-Workspaces

![Amazon-Workspaces](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/End+User+Computing/Amazon-Workspaces_dark-bg@4x.png)

バーチャルデスクトップ環境をAWSに構築できるサービス

---------------


# Game-Tech

![Game-Tech](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Game+Tech/Game-Tech_dark-bg@4x.png)



---------------


# Amazon-GameLift

![Amazon-GameLift](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Game+Tech/Amazon-GameLift_dark-bg@4x.png)

Gameサーバーのインフラ部分のサービス。セッション数のモニタリングなどマネジメント部分がある程度ついている。中身はEC2のサーバーが起動している

---------------


# Internet-of-Things

![Internet-of-Things](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/Internet-of-Things_dark-bg@4x.png)



---------------


# Amazon-FreeRTOS

![Amazon-FreeRTOS](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/Amazon-FreeRTOS_dark-bg@4x.png)

AWSの作ったマイクロコンピューター用OS。オープンソースのFreeRTOSをベースに、AWSのIoT CoreやGreenglassに簡単に接続できるようライブラリが組み込まれている

---------------


# AWS-IoT-1-Click

![AWS-IoT-1-Click](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-1-Click_dark-bg@4x.png)

1-clickで指定のラムダを動かせる物理ボタン

---------------


# AWS-IoT-Analytics

![AWS-IoT-Analytics](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Analytics_dark-bg@4x.png)

IoT Coreで取り込まれたデータ、さらにはS3やDynamoDBからもデータを取り込み、処理をしてデータの保存ができる。分析や可視化の役割も担う。Kinesis Analyticsはめっちゃリアルタイム性の高い分析に用いるが、IoT Analyticsの方は分単位とかでの分析に用いられる

---------------


# AWS-IoT-Button

![AWS-IoT-Button](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Button_dark-bg@4x.png)



---------------


# AWS-IoT-Core

![AWS-IoT-Core](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Core_dark-bg@4x.png)

IoTデバイスの管理、証明書の発行など、IoTの基礎になるサービス

---------------


# AWS-IoT-Device-Defender

![AWS-IoT-Device-Defender](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Device-Defender_dark-bg@4x.png)

多数のデバイスをリモートでテストできるサービス

---------------


# AWS-IoT-Events

![AWS-IoT-Events](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Events_dark-bg@4x.png)

IoTデバイスでなにか変化を検出した際に、なにかActionを起こせる

---------------


# AWS-IoT-Greengrass

![AWS-IoT-Greengrass](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Greengrass_dark-bg@4x.png)

IoTの各機能をクラウドからエッジで行う事ができる。ここでいうエッジとはAWSのクラウドのデバイスをつなぐ、拠点サーバみたいなものである。UbunteなどにGreenglass Coreをインストールすることができて、そのサーバ内でLambdaや推論を行うことができる。つまり完全にインターネットに繋がっていない環境でもクラウドを動かせる

---------------


# AWS-IoT-SiteWise

![AWS-IoT-SiteWise](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-SiteWise_dark-bg@4x.png)

工場の各センサーのデータをクラウドにアップするために、ゲートウェイサービスを展開し、データのクラウド上での可視化を可能にする。Snowball Edgeが動作環境としてあるため、借りればそれをゲートウェイにできる

---------------


# AWS-IoT-Things-Graph

![AWS-IoT-Things-Graph](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Internet+of+Things/AWS-IoT-Things-Graph_dark-bg@4x.png)

IoTのデータを入り口として各WEBサービス間連携のピタゴラスイッチをGUIで組める

---------------


# Machine-Learning

![Machine-Learning](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Machine-Learning_dark-bg@4x.png)



---------------


# Amazon-Comprehend

![Amazon-Comprehend](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Comprehend_dark-bg@4x.png)

自然言語処理。
テキストを投げると品詞分解や感情分解ができるAPI。日本語対応はまだ

---------------


# Amazon-Elastic-Inference

![Amazon-Elastic-Inference](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Elastic-Inference_dark-bg@4x.png)

EC2やSageMakerのインスタンスにGPU増しをattachできる機能

---------------


# Amazon-Forecast

![Amazon-Forecast](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Forecast_dark-bg@4x.png)

時系列データの予測をしてくれるサービス

---------------


# Amazon-Lex

![Amazon-Lex](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Lex_dark-bg@4x.png)

対話式インターフェースのチャットボットを構築できるサービス。Slack、Facebook、Kik、Twilio SMSが対応している

---------------


# Amazon-Personalize

![Amazon-Personalize](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Personalize_dark-bg@4x.png)

Amazon.comの経験を活かした、いい感じにパーソナライズをやってくれるマネージド・サービス

---------------


# Amazon-Polly

![Amazon-Polly](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Polly_dark-bg@4x.png)

テキストを音声で読み上げるサービス

---------------


# Amazon-Rekognition

![Amazon-Rekognition](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Rekognition_dark-bg@4x.png)

画像認識サービス。画像が何なのかのタグづけや、文字の認識ができる

---------------


# Amazon-SageMaker-Ground-Truth

![Amazon-SageMaker-Ground-Truth](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-SageMaker-Ground-Truth_dark-bg@4x.png)

教師ありデータの機械学習をする際のラベル付けサービス

---------------


# Amazon-SageMaker

![Amazon-SageMaker](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-SageMaker_dark-bg@4x.png)



---------------


# Amazon-Textract

![Amazon-Textract](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Textract_dark-bg@4x.png)



---------------


# Amazon-Transcribe

![Amazon-Transcribe](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Transcribe_dark-bg@4x.png)



---------------


# Amazon-Translate

![Amazon-Translate](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Amazon-Translate_dark-bg@4x.png)



---------------


# Apache-MXNet-on-AWS

![Apache-MXNet-on-AWS](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/Apache-MXNet-on-AWS_dark-bg@4x.png)

「効率」と「柔軟性」を両立した、オープンソースのディープラーニングフレームワーク

---------------


# AWS-Deep-Learning-AMIs

![AWS-Deep-Learning-AMIs](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/AWS-Deep-Learning-AMIs_dark-bg@4x.png)



---------------


# AWS-Deep-Learning-Containers

![AWS-Deep-Learning-Containers](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/AWS-Deep-Learning-Containers_dark-bg@4x.png)



---------------


# AWS-DeepLens

![AWS-DeepLens](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/AWS-DeepLens_dark-bg@4x.png)



---------------


# AWS-DeepRacer

![AWS-DeepRacer](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/AWS-DeepRacer_dark-bg@4x.png)



---------------


# TensorFlow-on-AWS

![TensorFlow-on-AWS](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Machine+Learning/TensorFlow-on-AWS_dark-bg@4x.png)



---------------


# Management-and-Governance

![Management-and-Governance](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/Management-and-Governance_dark-bg@4x.png)



---------------


# Amazon-CloudWatch

![Amazon-CloudWatch](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/Amazon-CloudWatch_dark-bg@4x.png)



---------------


# AWS-Auto-Scaling

![AWS-Auto-Scaling](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Auto-Scaling_dark-bg@4x.png)



---------------


# AWS-CloudFormation

![AWS-CloudFormation](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-CloudFormation_dark-bg@4x.png)



---------------


# AWS-CloudTrail

![AWS-CloudTrail](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-CloudTrail_dark-bg@4x.png)



---------------


# AWS-Command-Line-Interface

![AWS-Command-Line-Interface](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Command-Line-Interface_dark-bg@4x.png)



---------------


# AWS-Config

![AWS-Config](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Config_dark-bg@4x.png)



---------------


# AWS-Control-Tower

![AWS-Control-Tower](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Control-Tower_dark-bg@4x.png)



---------------


# AWS-License-Manager

![AWS-License-Manager](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-License-Manager_dark-bg@4x.png)

AWS で、また Microsoft、SAP、Oracle、IBM といったソフトウェアベンダーのオンプレミスサーバーで、ライセンスを簡単に管理できるようになるサービスです。
AWS License Manager により、管理者はライセンス契約の規約をエミュレートするカスタマイズされたライセンスルールを作成し、EC2 のインスタンスが起動するときにそれらのルールを適用できます。

---------------


# AWS-Managed-Services

![AWS-Managed-Services](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Managed-Services_dark-bg@4x.png)



---------------


# AWS-Management-Console

![AWS-Management-Console](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Management-Console_dark-bg@4x.png)



---------------


# AWS-OpsWorks

![AWS-OpsWorks](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-OpsWorks_dark-bg@4x.png)



---------------


# AWS-Organizations

![AWS-Organizations](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Organizations_dark-bg@4x.png)



---------------


# AWS-Personal-Health-Dashboard

![AWS-Personal-Health-Dashboard](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Personal-Health-Dashboard_dark-bg@4x.png)

現在利用中のサービスの状態を知ることができるダッシュボード

---------------


# AWS-Service-Catalog

![AWS-Service-Catalog](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Service-Catalog_dark-bg@4x.png)



---------------


# AWS-Systems-Manager

![AWS-Systems-Manager](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Systems-Manager_dark-bg@4x.png)



---------------


# AWS-Trusted-Advisor

![AWS-Trusted-Advisor](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Trusted-Advisor_dark-bg@4x.png)



---------------


# AWS-Well-Architected-Tool

![AWS-Well-Architected-Tool](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Management+&+Governance/AWS-Well-Architected-Tool_dark-bg@4x.png)



---------------


# Media-Services

![Media-Services](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Media+Services/Media-Services_dark-bg@4x.png)



---------------


# Amazon-Elastic-Transcoder

![Amazon-Elastic-Transcoder](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Media+Services/Amazon-Elastic-Transcoder_dark-bg@4x.png)



---------------


# Amazon-Kinesis-Video-Streams

![Amazon-Kinesis-Video-Streams](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Media+Services/Amazon-Kinesis-Video-Streams_dark-bg@4x.png)



---------------


# AWS-Elemental-MediaConnect

![AWS-Elemental-MediaConnect](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Media+Services/AWS-Elemental-MediaConnect_dark-bg@4x.png)



---------------


# AWS-Elemental-MediaLive

![AWS-Elemental-MediaLive](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Media+Services/AWS-Elemental-MediaLive_dark-bg@4x.png)



---------------


# Migration-and-Transfer

![Migration-and-Transfer](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/Migration-and-Transfer_dark-bg@4x.png)



---------------


# AWS-Application-Discovery-Service

![AWS-Application-Discovery-Service](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Application-Discovery-Service_dark-bg@4x.png)



---------------


# AWS-DataSync

![AWS-DataSync](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-DataSync_dark-bg@4x.png)

オンプレミスエージェントからデータ転送するサービス。
普通のS3APIを使うよりも10倍早い

---------------


# AWS-Migration-Hub

![AWS-Migration-Hub](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Migration-Hub_dark-bg@4x.png)



---------------


# AWS-Server-Migration-Service

![AWS-Server-Migration-Service](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Server-Migration-Service_dark-bg@4x.png)



---------------


# AWS-Snowball-Edge

![AWS-Snowball-Edge](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Snowball-Edge_dark-bg@4x.png)

snowball登場の一年後に出た上位品。
ネットワークインターフェースや、100TBの容量など向上し、ソフトウェアもその筐体内でS3やLambdaも動く。AWSに送り返す前にデータを加工して投入できる。
10日間使って3万円

---------------


# AWS-Snowball

![AWS-Snowball](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Snowball_dark-bg@4x.png)



---------------


# AWS-Snowmobile

![AWS-Snowmobile](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Snowmobile_dark-bg@4x.png)



---------------


# AWS-Transfer-for-SFTP

![AWS-Transfer-for-SFTP](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Migration+&+Transfer/AWS-Transfer-for-SFTP_dark-bg@4x.png)



---------------


# Mobile

![Mobile](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/Mobile_dark-bg@4x.png)



---------------


# Amazon-API-Gateway

![Amazon-API-Gateway](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/Amazon-API-Gateway_dark-bg@4x.png)



---------------


# Amazon-Pinpoint

![Amazon-Pinpoint](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/Amazon-Pinpoint_dark-bg@4x.png)

セグメント分けて通知するやつ

---------------


# AWS-Amplify

![AWS-Amplify](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/AWS-Amplify_dark-bg@4x.png)



---------------


# AWS-AppSync

![AWS-AppSync](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/AWS-AppSync_dark-bg@4x.png)



---------------


# AWS-Device-Farm

![AWS-Device-Farm](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Mobile/AWS-Device-Farm_dark-bg@4x.png)

いろんな機種でテストするやつ

---------------


# Networking-and-Content-Delivery

![Networking-and-Content-Delivery](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Networking-and-Content-Delivery_dark-bg@4x.png)



---------------


# Amazon-API-Gateway

![Amazon-API-Gateway](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-API-Gateway_dark-bg@4x.png)



---------------


# Amazon-CloudFront

![Amazon-CloudFront](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-CloudFront_dark-bg@4x.png)



---------------


# Amazon-Route-53

![Amazon-Route-53](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-Route-53_dark-bg@4x.png)



---------------


# Amazon-VPC

![Amazon-VPC](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Amazon-VPC_dark-bg@4x.png)



---------------


# AWS-App-Mesh

![AWS-App-Mesh](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-App-Mesh_dark-bg@4x.png)



---------------


# AWS-Client-VPN

![AWS-Client-VPN](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Client-VPN_dark-bg@4x.png)



---------------


# AWS-Cloud-Map

![AWS-Cloud-Map](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Cloud-Map_dark-bg@4x.png)



---------------


# AWS-Direct-Connect

![AWS-Direct-Connect](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Direct-Connect_dark-bg@4x.png)



---------------


# AWS-Global-Accelerator

![AWS-Global-Accelerator](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Global-Accelerator_dark-bg@4x.png)



---------------


# AWS-PrivateLink

![AWS-PrivateLink](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-PrivateLink_dark-bg@4x.png)



---------------


# AWS-Site-to-Site-VPN

![AWS-Site-to-Site-VPN](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Site-to-Site-VPN_dark-bg@4x.png)



---------------


# AWS-Transit-Gateway

![AWS-Transit-Gateway](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/AWS-Transit-Gateway_dark-bg@4x.png)



---------------


# Elastic-Load-Balancing

![Elastic-Load-Balancing](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Networking+&+Content+Delivery/Elastic-Load-Balancing_dark-bg@4x.png)



---------------


# Robotics

![Robotics](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Robotics/Robotics_dark-bg@4x.png)



---------------


# AWS-RoboMaker

![AWS-RoboMaker](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Robotics/AWS-RoboMaker_dark-bg@4x.png)



---------------


# Satellite

![Satellite](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Satellite/Satellite_dark-bg@4x.png)



---------------


# AWS-Ground-Station

![AWS-Ground-Station](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Satellite/AWS-Ground-Station_dark-bg@4x.png)



---------------


# Security-Identity-and-Compliance

![Security-Identity-and-Compliance](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Security-Identity-and-Compliance_dark-bg@4x.png)



---------------


# Amazon-Cloud-Directory

![Amazon-Cloud-Directory](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-Cloud-Directory_dark-bg@4x.png)



---------------


# Amazon-Cognito

![Amazon-Cognito](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-Cognito_dark-bg@4x.png)



---------------


# Amazon-GuardDuty

![Amazon-GuardDuty](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-GuardDuty_dark-bg@4x.png)



---------------


# Amazon-Inspector

![Amazon-Inspector](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-Inspector_dark-bg@4x.png)



---------------


# Amazon-Macie

![Amazon-Macie](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/Amazon-Macie_dark-bg@4x.png)



---------------


# AWS-Artifact

![AWS-Artifact](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Artifact_dark-bg@4x.png)



---------------


# AWS-Certificate-Manager

![AWS-Certificate-Manager](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Certificate-Manager_dark-bg@4x.png)



---------------


# AWS-CloudHSM

![AWS-CloudHSM](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-CloudHSM_dark-bg@4x.png)



---------------


# AWS-Directory-Service

![AWS-Directory-Service](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Directory-Service_dark-bg@4x.png)



---------------


# AWS-Firewall-Manager

![AWS-Firewall-Manager](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Firewall-Manager_dark-bg@4x.png)



---------------


# AWS-Key-Management-Service

![AWS-Key-Management-Service](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Key-Management-Service_dark-bg@4x.png)



---------------


# AWS-Resource-Access-Manager

![AWS-Resource-Access-Manager](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Resource-Access-Manager_dark-bg@4x.png)



---------------


# AWS-Secrets-Manager

![AWS-Secrets-Manager](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Secrets-Manager_dark-bg@4x.png)



---------------


# AWS-Security-Hub

![AWS-Security-Hub](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Security-Hub_dark-bg@4x.png)



---------------


# AWS-Shield

![AWS-Shield](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Shield_dark-bg@4x.png)



---------------


# AWS-Single-Sign-On

![AWS-Single-Sign-On](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-Single-Sign-On_dark-bg@4x.png)



---------------


# AWS-WAF

![AWS-WAF](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Security,+Identity,+&+Compliance/AWS-WAF_dark-bg@4x.png)



---------------


# Storage

![Storage](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Storage_dark-bg@4x.png)



---------------


# Amazon-Elastic-Block-Store-EBS

![Amazon-Elastic-Block-Store-EBS](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-Elastic-Block-Store-EBS_dark-bg@4x.png)



---------------


# Amazon-Elastic-File-System_EFS

![Amazon-Elastic-File-System_EFS](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-Elastic-File-System_EFS_dark-bg@4x.png)



---------------


# Amazon-FSx-for-Lustre

![Amazon-FSx-for-Lustre](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-FSx-for-Lustre_dark-bg@4x.png)



---------------


# Amazon-FSx-for-Windows-File-Server

![Amazon-FSx-for-Windows-File-Server](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-FSx-for-Windows-File-Server_dark-bg@4x.png)



---------------


# Amazon-FSx

![Amazon-FSx](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-FSx_dark-bg@4x.png)



---------------


# Amazon-S3-Glacier

![Amazon-S3-Glacier](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/Amazon-S3-Glacier_dark-bg@4x.png)



---------------


# AWS-Backup

![AWS-Backup](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Backup_dark-bg@4x.png)



---------------


# AWS-Snowball-Edge

![AWS-Snowball-Edge](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Snowball-Edge_dark-bg@4x.png)



---------------


# AWS-Snowball

![AWS-Snowball](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Snowball_dark-bg@4x.png)



---------------


# AWS-Snowmobile

![AWS-Snowmobile](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Snowmobile_dark-bg@4x.png)



---------------


# AWS-Storage-Gateway

![AWS-Storage-Gateway](https://sakapun-aws-icon.s3-ap-northeast-1.amazonaws.com/PNG+Dark/Storage/AWS-Storage-Gateway_dark-bg@4x.png)



---------------
